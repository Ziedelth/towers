package fr.ziedelth.towers.kits;

import fr.ziedelth.towers.builders.ItemBuilder;
import fr.ziedelth.towers.utils.Kit;
import fr.ziedelth.towers.utils.TowerPlayer;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;

public class ArcherKit extends Kit {
    public ArcherKit(Player player) {
        super(player);
        TowerPlayer towerPlayer = TowerPlayer.get(player);

        this.setHelmet(new ItemBuilder(Material.LEATHER_HELMET).canInteract(true).color(towerPlayer.getTeam().getArmorColor()).unbreakable(true).addItemFlag(ItemFlag.HIDE_UNBREAKABLE).build());
        this.setChestplate(new ItemBuilder(Material.CHAINMAIL_CHESTPLATE).canInteract(true).unbreakable(true).addItemFlag(ItemFlag.HIDE_UNBREAKABLE).build());
        this.setBoots(new ItemBuilder(Material.LEATHER_BOOTS).canInteract(true).color(towerPlayer.getTeam().getArmorColor()).unbreakable(true).addItemFlag(ItemFlag.HIDE_UNBREAKABLE).build());
        this.addItems(new ItemBuilder(Material.WOOD_SWORD).canInteract(true).unbreakable(true).addItemFlag(ItemFlag.HIDE_UNBREAKABLE).build());
        this.addItems(new ItemBuilder(Material.BOW).canInteract(true).unbreakable(true).addItemFlag(ItemFlag.HIDE_UNBREAKABLE).build());
        this.addItems(new ItemBuilder(Material.ARROW).canInteract(true).amount(16).build());
    }
}
