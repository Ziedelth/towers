package fr.ziedelth.towers.kits;

import fr.ziedelth.towers.builders.ItemBuilder;
import fr.ziedelth.towers.utils.Kit;
import fr.ziedelth.towers.utils.TowerPlayer;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class SpyKit extends Kit {
    public SpyKit(Player player) {
        super(player);
        TowerPlayer towerPlayer = TowerPlayer.get(player);

        this.setHelmet(new ItemBuilder(Material.LEATHER_HELMET).canInteract(true).color(towerPlayer.getTeam().getArmorColor()).unbreakable(true).addItemFlag(ItemFlag.HIDE_UNBREAKABLE).build());
        this.setLeggings(new ItemBuilder(Material.CHAINMAIL_LEGGINGS).canInteract(true).unbreakable(true).addItemFlag(ItemFlag.HIDE_UNBREAKABLE).build());
        this.setBoots(new ItemBuilder(Material.CHAINMAIL_BOOTS).canInteract(true).unbreakable(true).addItemFlag(ItemFlag.HIDE_UNBREAKABLE).build());
        this.addItems(new ItemBuilder(Material.WOOD_SWORD).canInteract(true).unbreakable(true).addItemFlag(ItemFlag.HIDE_UNBREAKABLE).build());
        player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 99999, 0));
    }
}
