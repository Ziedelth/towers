package fr.ziedelth.towers.builders;

import org.bukkit.entity.Player;

public interface PlayerRunnableBuilder {
    void run(Player player);
}
