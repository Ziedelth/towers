package fr.ziedelth.towers.utils;

import fr.ziedelth.towers.builders.ItemBuilder;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public enum OreType {
    IRON(15, new ItemBuilder(Material.IRON_INGOT).build()),
    DIAMOND(60, new ItemBuilder(Material.DIAMOND).build()),
    EXP(45, new ItemBuilder(Material.EXP_BOTTLE).build());

    private final int seconds;
    private final ItemStack itemStack;
    private Location[] locations;

    OreType(int seconds, ItemStack itemStack) {
        this.seconds = seconds;
        this.itemStack = itemStack;
    }

    public static void enable() {
        IRON.setLocations(new Location(Bukkit.getWorlds().get(0), 0.5, 205, 1166.5), new Location(Bukkit.getWorlds().get(0), 0.5, 205, 1138.5));
        DIAMOND.setLocations(new Location(Bukkit.getWorlds().get(0), 0.5, 205, 1152.5));
        EXP.setLocations(new Location(Bukkit.getWorlds().get(0), 0.5, 205, 1152.5));
    }

    public int getSeconds() {
        return seconds;
    }

    public ItemStack getItemStack() {
        return itemStack;
    }

    public Location[] getLocations() {
        return locations;
    }

    private void setLocations(Location... locations) {
        this.locations = locations;
    }
}
