package fr.ziedelth.towers.utils;

import fr.ziedelth.towers.Towers;
import fr.ziedelth.towers.builders.ScoreboardBuilder;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scoreboard.DisplaySlot;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class TowerPlayer {
    private static final List<TowerPlayer> players = new ArrayList<>();
    private final Player player;
    private ScoreboardBuilder scoreboardBuilder;
    private int kills;
    private int deaths;
    private Teams team;
    private int score;
    private boolean invulnerable = false;
    private KitType kitType = KitType.TANK;
    private Kit kit;
    private BossBar bossBar;

    public TowerPlayer(Player player) {
        this.player = player;
    }

    public static List<TowerPlayer> getPlayers() {
        return players;
    }

    public static TowerPlayer get(Player player) {
        TowerPlayer towerPlayer = null;

        for (TowerPlayer players : getPlayers()) {
            if (players.getPlayer().getUniqueId().equals(player.getUniqueId())) {
                towerPlayer = players;
                break;
            }
        }

        if (towerPlayer == null) {
            towerPlayer = new TowerPlayer(player);
            getPlayers().add(towerPlayer);
        }

        return towerPlayer;
    }

    public static List<TowerPlayer> getPlayersInTeam(Teams team) {
        return getPlayers().stream().filter(towerPlayer -> towerPlayer.getTeam() != null && towerPlayer.getTeam().equals(team)).collect(Collectors.toList());
    }

    public static int getScoreOf(Teams team) {
        return getPlayersInTeam(team).stream().mapToInt(TowerPlayer::getScore).sum();
    }

    public Player getPlayer() {
        return player;
    }

    public void init() {
        boolean joining = this.scoreboardBuilder == null;

        if (joining) {
            // Scoreboard
            this.scoreboardBuilder = new ScoreboardBuilder("§6Towers");
            this.scoreboardBuilder.addObjective(DisplaySlot.SIDEBAR);
            this.scoreboardBuilder.addHealthObjective(DisplaySlot.PLAYER_LIST);
            Towers.getInstance().getTeams().forEach(team -> this.scoreboardBuilder.addTeam(team.getTeamBuilder()));

            getPlayers().forEach(players -> {
                if (players.getTeam() == null) return;
                this.scoreboardBuilder.addTo(players.getTeam().getName(), players.getPlayer());
            });

            this.scoreboardBuilder.buildTo(this.player);

            // Bossbar
            this.bossBar = Bukkit.createBossBar("", BarColor.WHITE, BarStyle.SOLID);
            this.bossBar.setProgress(1f);
            this.bossBar.addPlayer(this.player);
        }

        if (GameStates.isState(GameStates.LOBBY)) {
            if (joining) this.player.teleport(Towers.getInstance().getLobbyLocation());
            if (Bukkit.getOnlinePlayers().size() >= Towers.getInstance().getMinPlayers())
                Towers.getInstance().getLobbyManager().start();
        } else if (GameStates.isState(GameStates.PREPARATION)) {
            if (this.team == null) this.joinTeam();
            if (joining) this.player.teleport(Towers.getInstance().getLobbyLocation());
            TaskUtils.runTaskLater(() -> this.player.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 20 * (Towers.getInstance().getCurrentManager().getSeconds() + 1), 1)));
        } else if (GameStates.isState(GameStates.GAME)) {
            if (this.team == null) this.joinTeam();
            this.player.teleport(this.team.getSpawnLocation());
            this.invulnerable = true;
            TaskUtils.runTaskLater(() -> this.invulnerable = false, 20 * 5L);
        } else if (GameStates.isState(GameStates.END)) {
            if (joining) this.player.teleport(Towers.getInstance().getLobbyLocation());
        }

        InventoryUtils.setInventory(this.player);
        getPlayers().forEach(TowerPlayer::update);
    }

    public void update() {
        this.scoreboardBuilder.resetDisplayScore();
        this.scoreboardBuilder.updateHealth();
        this.scoreboardBuilder.addDisplayScore("§6Phase: §e" + GameStates.getState().name());
        int seconds = Towers.getInstance().getCurrentManager().getSeconds();
        this.scoreboardBuilder.addDisplayScore("§6Temps restant: §e" + String.format("%02d:%02d", (seconds / 60), (seconds % 60)));
        this.scoreboardBuilder.addDisplayScore("§6Joueur(s): §e" + Bukkit.getOnlinePlayers().size());
        this.scoreboardBuilder.addDisplayScore("§0");
        this.scoreboardBuilder.addDisplayScore("§7-- Informations --");
        this.scoreboardBuilder.addDisplayScore("§6Equipe: " + (this.team != null ? this.team.getName() : "§7???"));
        this.scoreboardBuilder.addDisplayScore("§6Kit: §e" + (this.kitType != null ? this.kitType.getName() : "§7???"));
        this.scoreboardBuilder.addDisplayScore("§6Elimination(s): §e" + this.getKills());
        this.scoreboardBuilder.addDisplayScore("§6Mort(s): §e" + this.getDeaths());
        this.scoreboardBuilder.addDisplayScore("§6Point(s) marqué(s): §e" + this.getScore());
        this.scoreboardBuilder.addDisplayScore("§1");
        this.scoreboardBuilder.addDisplayScore("§7-- Objectifs: " + Towers.getInstance().getMaxScore() + " --");
        Towers.getInstance().getTeams().forEach(team -> this.scoreboardBuilder.addDisplayScore(team.getName() + "§f: §e" + getScoreOf(team)));

        if (GameStates.isState(GameStates.GAME)) {
            Teams team = Towers.getInstance().getTeamWithTheHighestScore();
            this.bossBar.setProgress((double) getScoreOf(team) / (double) Towers.getInstance().getMaxScore());
            this.bossBar.setStyle(BarStyle.SEGMENTED_10);

            // Egalité
            if (Towers.getInstance().allTeamsHaveTheSameScore()) {
                this.bossBar.setTitle("§fEgalité");
                this.bossBar.setColor(BarColor.WHITE);
            } else {
                this.bossBar.setTitle(team.getName());
                this.bossBar.setColor(team.getBarColor());
            }
        } else {
            this.bossBar.setTitle("");
            this.bossBar.setColor(BarColor.WHITE);
            this.bossBar.setProgress((double) seconds / (double) Towers.getInstance().getCurrentManager().getMaxSeconds());
            this.bossBar.setStyle(BarStyle.SOLID);
        }
    }

    public int getKills() {
        return kills;
    }

    public void addKill() {
        this.kills++;
    }

    public int getDeaths() {
        return deaths;
    }

    public void addDeath() {
        this.deaths++;
    }

    public Teams getTeam() {
        return team;
    }

    public void joinTeam() {
        if (Towers.getInstance().allTeamsHaveTheSameNumberPlayers()) {
            int random_int = new Random().nextInt(Towers.getInstance().getTeams().size());
            Teams random_team = Towers.getInstance().getTeams().get(random_int);
            this.setTeam(random_team, true);
        } else this.setTeam(Towers.getInstance().getSmallerTeam(), true);
    }

    public void setTeam(Teams team, boolean messages) {
        this.team = team;
        this.score = 0;
        this.update();
        if (this.team == null) return;

        if (messages) {
            this.player.sendMessage(ChatUtils.getTeamJoinMessage(this.team));

            getPlayers().forEach(players -> {
                players.scoreboardBuilder.addTo(this.team.getName(), this.player);
                if (players.getTeam() == null) return;
                this.scoreboardBuilder.addTo(players.getTeam().getName(), players.getPlayer());
            });
        }
    }

    public boolean isInScoreLocation(Teams team) {
        Location location = this.player.getLocation();
        int minX = Math.min(team.getPointFrom().getBlockX(), team.getPointTo().getBlockX()), maxX = Math.max(team.getPointFrom().getBlockX(), team.getPointTo().getBlockX());
        int minY = Math.min(team.getPointFrom().getBlockY(), team.getPointTo().getBlockY()), maxY = Math.max(team.getPointFrom().getBlockY(), team.getPointTo().getBlockY());
        int minZ = Math.min(team.getPointFrom().getBlockZ(), team.getPointTo().getBlockZ()), maxZ = Math.max(team.getPointFrom().getBlockZ(), team.getPointTo().getBlockZ());
        return (location.getBlockX() >= minX && location.getBlockX() <= maxX) && (location.getBlockY() >= minY && location.getBlockY() <= maxY) && (location.getBlockZ() >= minZ && location.getBlockZ() <= maxZ);
    }

    public int getScore() {
        return score;
    }

    public void addScore() {
        this.score++;
        this.player.teleport(this.team.getSpawnLocation());
        this.player.sendMessage(ChatUtils.addPersonnalScoreMessage());

        Bukkit.getOnlinePlayers().forEach(players -> {
            players.sendMessage(ChatUtils.addTeamScoreMessage(this.team));
            players.playSound(players.getLocation(), Sound.ENTITY_LIGHTNING_THUNDER, 1f, 1f);
        });

        if (getScoreOf(this.team) >= Towers.getInstance().getMaxScore()) Towers.getInstance().getGameManager().finish();
    }

    public boolean isInvulnerable() {
        return invulnerable;
    }

    public KitType getKitType() {
        return kitType;
    }

    public void setKitType(KitType kitType) {
        this.kitType = kitType;
    }

    public Kit getKit() {
        return kit;
    }

    public void setKitInventory() {
        try {
            this.kit = this.kitType.getKitClass().getConstructor(Player.class).newInstance(this.player);
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            e.printStackTrace();
        }
    }
}
