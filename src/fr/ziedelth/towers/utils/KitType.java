package fr.ziedelth.towers.utils;

import fr.ziedelth.towers.builders.ItemBuilder;
import fr.ziedelth.towers.kits.ArcherKit;
import fr.ziedelth.towers.kits.SpyKit;
import fr.ziedelth.towers.kits.TankKit;
import org.bukkit.Material;

public enum KitType {
    TANK("Tank", new ItemBuilder(Material.IRON_CHESTPLATE), TankKit.class, "§a- Grosse protection", "§a- Grosse attaque", "§c- Mobilité réduite"),
    ARCHER("Archer", new ItemBuilder(Material.BOW), ArcherKit.class, "§a- Apparation avec arc et flèches", "§c- Faible armure", "§c- Faible attaque"),
    SPY("Espion", new ItemBuilder(Material.FEATHER), SpyKit.class, "§a- Mobilité augmentée", "§c- Faible armure", "§c- Faible attaque"),
    ;

    private final String name;
    private final ItemBuilder icon;
    private final Class<? extends Kit> kitClass;

    KitType(String name, ItemBuilder icon, Class<? extends Kit> kitClass, String... lore) {
        this.name = name;
        this.icon = icon;
        this.icon.name("§6" + this.name);
        this.icon.lore(lore);
        this.kitClass = kitClass;
    }

    public String getName() {
        return name;
    }

    public ItemBuilder getIcon() {
        return icon;
    }

    public Class<? extends Kit> getKitClass() {
        return kitClass;
    }
}
