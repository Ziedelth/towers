package fr.ziedelth.towers.utils;

import fr.ziedelth.towers.Towers;
import fr.ziedelth.towers.builders.ItemBuilder;
import fr.ziedelth.towers.builders.VirtualButton;
import fr.ziedelth.towers.builders.VirtualInventoryBuilder;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import java.util.Arrays;

public class InventoryUtils {
    public static void clearInventory(Player player) {
        PlayerInventory inventory = player.getInventory();
        inventory.clear();
        inventory.setArmorContents(new ItemStack[4]);
        player.getActivePotionEffects().forEach(potionEffect -> player.removePotionEffect(potionEffect.getType()));
    }

    public static void setInventory(Player player) {
        clearInventory(player);

        TowerPlayer towerPlayer = TowerPlayer.get(player);
        PlayerInventory inventory = player.getInventory();
        player.setGameMode(GameStates.isState(GameStates.GAME) ? GameMode.SURVIVAL : GameMode.ADVENTURE);
        double maxHealth = 20D;
        player.getAttribute(Attribute.GENERIC_MAX_HEALTH).setBaseValue(maxHealth);
        player.setHealth(maxHealth);
        player.setFoodLevel(20);
        player.setExp(0f);
        player.setLevel(0);
        boolean canFly = GameStates.isState(GameStates.END);
        player.setAllowFlight(canFly);
        player.setFlying(canFly);
        player.getAttribute(Attribute.GENERIC_ATTACK_SPEED).setBaseValue(32);

        if (GameStates.isState(GameStates.LOBBY)) {
            inventory.setItem(0, new ItemBuilder(Material.BOOK).name("§6Choisir une équipe").canInteract(true).interact((clicker) -> {
                if (!clicker.getUniqueId().equals(player.getUniqueId())) return;

                VirtualInventoryBuilder builder = new VirtualInventoryBuilder("Choisir une équipe", 9);

                Towers.getInstance().getTeams().forEach(teams -> {
                    VirtualButton button = new VirtualButton(new ItemBuilder(Material.PAPER).name(teams.getName()).build());

                    button.setOnActionListener(() -> {
                        clicker.closeInventory();
                        joinTeam(clicker, teams);
                    });

                    builder.addChildren(button);
                });

                clicker.openInventory(builder.build());
            }).build());

            inventory.setItem(1, new ItemBuilder(Material.PAPER).name("§6Choisir un kit").canInteract(true).interact((clicker) -> {
                if (!clicker.getUniqueId().equals(player.getUniqueId())) return;

                VirtualInventoryBuilder builder = new VirtualInventoryBuilder("Choisir un kit", 9);

                Arrays.asList(KitType.values()).forEach(kits -> {
                    VirtualButton button = new VirtualButton(new ItemBuilder(kits.getIcon()).build());

                    button.setOnActionListener(() -> {
                        clicker.closeInventory();
                        towerPlayer.setKitType(kits);
                        clicker.sendMessage(ChatUtils.getSelectKitMessage(kits));
                    });

                    builder.addChildren(button);
                });

                clicker.openInventory(builder.build());
            }).build());
        } else if (GameStates.isState(GameStates.GAME)) {
            towerPlayer.setKitInventory();
        }
    }

    private static boolean joinTeam(Player player, Teams team) {
        TowerPlayer tP = TowerPlayer.get(player);
        Teams tTeam = tP.getTeam();

        // Si le joueur a déjà rejoint une équipe et cherche à rejoindre la même équipe
        if (tTeam != null && tTeam.equals(team)) {
            player.sendMessage(ChatUtils.getSameTeamMessage());
            return false;
        }

        // Si toutes les équipes ont le même nombre de joueurs
        if (Towers.getInstance().allTeamsHaveTheSameNumberPlayers()) {
            // Si le joueur est déjà dans une équipe
            if (tTeam != null) {
                // Détection si le joueur peut rejoindre l'équipe
                tP.setTeam(null, false);
                if (!joinTeam(player, team)) tP.setTeam(tTeam, false);
            }
            // Si le joueur n'avait pas rejoint d'équipe
            else {
                tP.setTeam(team, true);
                return true;
            }
        }
        // Si les équipes n'ont pas le même nombre joueurs
        else {
            // Si le joueur cheche à rejoindre l'équipe qui a le plus de joueur
            if (team.equals(Towers.getInstance().getBiggerTeam())) {
                player.sendMessage(ChatUtils.getCannotJoinTeamMessage());
                return false;
            }
            // Attribution de l'équipe au joueur
            else {
                tP.setTeam(team, true);
                return true;
            }
        }

        return false;
    }
}
