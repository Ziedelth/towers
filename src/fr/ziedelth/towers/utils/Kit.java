package fr.ziedelth.towers.utils;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Kit {
    private final List<ItemStack> kit_items = new ArrayList<>();
    private final Player player;

    public Kit(Player player) {
        this.player = player;
    }

    public void addItems(ItemStack... items) {
        this.kit_items.addAll(Arrays.asList(items));
        this.player.getInventory().addItem(items);
    }

    public void setHelmet(ItemStack item) {
        this.kit_items.add(item);
        this.player.getInventory().setHelmet(item);
    }

    public void setChestplate(ItemStack item) {
        this.kit_items.add(item);
        this.player.getInventory().setChestplate(item);
    }

    public void setLeggings(ItemStack item) {
        this.kit_items.add(item);
        this.player.getInventory().setLeggings(item);
    }

    public void setBoots(ItemStack item) {
        this.kit_items.add(item);
        this.player.getInventory().setBoots(item);
    }

    public boolean isSimilar(ItemStack itemStack) {
        for (ItemStack kitItem : this.kit_items) if (itemStack.isSimilar(kitItem)) return true;
        return false;
    }
}
