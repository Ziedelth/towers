package fr.ziedelth.towers.utils;

import fr.ziedelth.towers.Towers;
import org.bukkit.Bukkit;

public class TaskUtils {
    public static void runTaskLater(Runnable runnable) {
        runTaskLater(runnable, 1L);
    }

    public static void runTaskLater(Runnable runnable, long delay) {
        Bukkit.getScheduler().runTaskLater(Towers.getInstance(), runnable, delay);
    }
}
