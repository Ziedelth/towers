package fr.ziedelth.towers.utils.timers;

import fr.ziedelth.towers.Towers;
import fr.ziedelth.towers.utils.Timer;
import fr.ziedelth.towers.utils.*;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Item;
import org.bukkit.util.Vector;

import java.util.*;

public class GameManager extends Timer {
    private final Map<OreType, Integer> oreSeconds = new HashMap<>();

    public GameManager() {
        this.setMaxSeconds(1200);
    }

    @Override
    public void start() {
        super.start();
        Arrays.asList(OreType.values()).forEach(oreType -> this.oreSeconds.put(oreType, 0));
        GameStates.setState(GameStates.GAME);
        TowerPlayer.getPlayers().forEach(TowerPlayer::init);
    }

    public void finish() {
        this.stop();
        Towers.getInstance().getEndManager().start();
        TowerPlayer.getPlayers().forEach(TowerPlayer::update);

        // Egalite
        if (Towers.getInstance().allTeamsHaveTheSameScore())
            Bukkit.getOnlinePlayers().forEach(players -> players.sendMessage(ChatUtils.getDrawMessage()));
        else {
            TowerPlayer.getPlayers().forEach(towerPlayer -> {
                // Gagnant
                if (towerPlayer.getTeam().equals(Towers.getInstance().getTeamWithTheHighestScore()))
                    towerPlayer.getPlayer().sendMessage(ChatUtils.getWinMessage());
                    // Perdant
                else towerPlayer.getPlayer().sendMessage(ChatUtils.getLoseMessage());
            });
        }
    }

    @Override
    public void run() {
        int seconds = this.getSeconds();

        Arrays.asList(OreType.values()).forEach(oreType -> {
            int oreSeconds = this.oreSeconds.get(oreType) + 1;

            if (oreSeconds >= oreType.getSeconds()) {
                oreSeconds = 0;
                Arrays.asList(oreType.getLocations()).forEach(location -> {
                    Item item = location.getWorld().dropItem(location, oreType.getItemStack());
                    item.teleport(location);
                    item.setVelocity(new Vector(0f, 0f, 0f));
                });
            }

            this.oreSeconds.replace(oreType, oreSeconds);
        });

        // Taille d'équipe non respecté
        if (!this.isValid()) {
            // Equilibrage de l'équipe qui a le plus de joueur
            this.balancing();
            Bukkit.getOnlinePlayers().forEach(players -> players.sendMessage(ChatUtils.getBalancingMessage()));
        }

        TowerPlayer.getPlayers().forEach(TowerPlayer::update);

        if (seconds == 60 || seconds == 30 || seconds == 10 || (seconds <= 5 && seconds >= 1)) {
            Bukkit.getOnlinePlayers().forEach(player -> {
                player.playSound(player.getLocation(), Sound.UI_BUTTON_CLICK, 1f, 1f);
                player.sendMessage(ChatUtils.getEndMessage(seconds));
                player.sendTitle("§6" + seconds, "§eLA PARTIE SE TERMINE BIENTÔT !", 0, 20, 0);
            });
        }

        if (seconds <= 0) {
            this.finish();
        }
    }

    private boolean isValid() {
        Teams tBigest = Towers.getInstance().getBiggerTeam();
        int length = TowerPlayer.getPlayersInTeam(tBigest).size();

        for (Teams teams : Towers.getInstance().getTeams()) {
            if (teams.equals(tBigest)) continue;
            int tLength = TowerPlayer.getPlayersInTeam(teams).size();
            if (tLength < length - 1) return false;
        }

        return true;
    }

    private void balancing() {
        Teams tBigest = Towers.getInstance().getBiggerTeam();
        List<TowerPlayer> players = TowerPlayer.getPlayersInTeam(tBigest);
        TowerPlayer randomPlayer = players.get(new Random().nextInt(players.size()));
        randomPlayer.joinTeam();
        randomPlayer.init();
        // Boucle infinie si les équipes ne sont toujours pas équilibrées
        if (!this.isValid()) this.balancing();
    }
}
