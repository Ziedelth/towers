package fr.ziedelth.towers.utils.timers;

import fr.ziedelth.towers.utils.GameStates;
import fr.ziedelth.towers.utils.Timer;
import fr.ziedelth.towers.utils.TowerPlayer;
import org.bukkit.Bukkit;

public class EndManager extends Timer {
    public EndManager() {
        this.setMaxSeconds(10);
    }

    @Override
    public void start() {
        super.start();
        GameStates.setState(GameStates.END);
        TowerPlayer.getPlayers().forEach(TowerPlayer::init);
    }

    @Override
    public void run() {
        int seconds = this.getSeconds();
        TowerPlayer.getPlayers().forEach(TowerPlayer::update);

        if (seconds <= 0) {
            this.stop();
            Bukkit.spigot().restart();
        }
    }
}
