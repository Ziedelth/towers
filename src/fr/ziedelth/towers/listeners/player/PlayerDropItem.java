package fr.ziedelth.towers.listeners.player;

import fr.ziedelth.towers.utils.GameStates;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerDropItemEvent;

public class PlayerDropItem implements Listener {
    @EventHandler
    public void onPlayerDrop(PlayerDropItemEvent event) {
        event.setCancelled(!GameStates.isState(GameStates.GAME));
    }
}
