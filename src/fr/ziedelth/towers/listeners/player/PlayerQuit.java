package fr.ziedelth.towers.listeners.player;

import fr.ziedelth.towers.utils.ChatUtils;
import fr.ziedelth.towers.utils.TowerPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerQuit implements Listener {
    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        TowerPlayer towerPlayer = TowerPlayer.get(player);
        TowerPlayer.getPlayers().remove(towerPlayer);
        event.setQuitMessage(ChatUtils.getPlayerQuitMessage(player));
    }
}
