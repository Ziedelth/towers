package fr.ziedelth.towers.listeners.player;

import fr.ziedelth.towers.Towers;
import fr.ziedelth.towers.utils.Teams;
import fr.ziedelth.towers.utils.TowerPlayer;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

public class BlockBreak implements Listener {
    @EventHandler
    public void onBlockBreak(BlockBreakEvent event) {
        Player player = event.getPlayer();
        TowerPlayer towerPlayer = TowerPlayer.get(player);
        Location location = event.getBlock().getLocation();

        for (Teams teams : Towers.getInstance().getTeams()) {
            if (this.isInScoreLocation(location, teams)) {
                event.setCancelled(true);
                break;
            }

            if (teams.equals(towerPlayer.getTeam())) continue;

            if (teams.getSpawnLocation().distance(location) <= Towers.getInstance().getPlayersSpawnProtectDistance()) {
                event.setCancelled(true);
                break;
            }
        }
    }

    public boolean isInScoreLocation(Location location, Teams team) {
        int minX = Math.min(team.getPointFrom().getBlockX(), team.getPointTo().getBlockX()), maxX = Math.max(team.getPointFrom().getBlockX(), team.getPointTo().getBlockX());
        int minY = Math.min(team.getPointFrom().getBlockY(), team.getPointTo().getBlockY()), maxY = Math.max(team.getPointFrom().getBlockY(), team.getPointTo().getBlockY());
        int minZ = Math.min(team.getPointFrom().getBlockZ(), team.getPointTo().getBlockZ()), maxZ = Math.max(team.getPointFrom().getBlockZ(), team.getPointTo().getBlockZ());
        return (location.getBlockX() >= minX && location.getBlockX() <= maxX) && (location.getBlockY() >= minY && location.getBlockY() <= maxY) && (location.getBlockZ() >= minZ && location.getBlockZ() <= maxZ);
    }
}
