package fr.ziedelth.towers.listeners.player;

import fr.ziedelth.towers.builders.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.inventory.EnchantingInventory;
import org.bukkit.inventory.Inventory;

public class InventoryOpen implements Listener {
    @EventHandler
    public void onInventoryOpen(InventoryOpenEvent event) {
        Inventory inventory = event.getInventory();

        if (inventory instanceof EnchantingInventory) {
            EnchantingInventory enchantingInventory = (EnchantingInventory) inventory;
            enchantingInventory.setItem(1, new ItemBuilder(Material.INK_SACK).canInteract(false).durability((short) 4).amount(3).build());
        }
    }
}
