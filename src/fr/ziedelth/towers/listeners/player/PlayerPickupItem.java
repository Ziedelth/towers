package fr.ziedelth.towers.listeners.player;

import fr.ziedelth.towers.utils.GameStates;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerPickupItemEvent;

public class PlayerPickupItem implements Listener {
    @EventHandler
    public void onPlayerPickupItem(PlayerPickupItemEvent event) {
        event.setCancelled(!GameStates.isState(GameStates.GAME));
    }
}
