package fr.ziedelth.towers.listeners.player;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.EnchantingInventory;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

public class InventoryClose implements Listener {
    @EventHandler
    public void onInventoryClose(InventoryCloseEvent event) {
        PlayerInventory playerInventory = event.getPlayer().getInventory();
        Inventory inventory = event.getInventory();
        // container.crafting

        if (inventory instanceof EnchantingInventory) {
            EnchantingInventory enchantingInventory = (EnchantingInventory) inventory;
            enchantingInventory.setItem(1, null);
        } else if (inventory.getName().equalsIgnoreCase("container.crafting")) {
            ItemStack offHandItem = playerInventory.getItemInOffHand();

            if (offHandItem != null && !offHandItem.getType().equals(Material.AIR)) {
                playerInventory.setItemInOffHand(null);
                playerInventory.addItem(offHandItem);
            }
        }
    }
}
