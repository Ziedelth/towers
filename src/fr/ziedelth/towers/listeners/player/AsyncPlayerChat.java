package fr.ziedelth.towers.listeners.player;

import fr.ziedelth.towers.builders.ScoreboardBuilder;
import fr.ziedelth.towers.utils.TowerPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class AsyncPlayerChat implements Listener {
    @EventHandler
    public void onAsyncPlayerChat(AsyncPlayerChatEvent event) {
        Player player = event.getPlayer();
        TowerPlayer towerPlayer = TowerPlayer.get(player);
        String message = event.getMessage();
        ScoreboardBuilder.TeamBuilder teamBuilder = towerPlayer.getTeam().getTeamBuilder();

        event.setFormat(teamBuilder.getPrefix() + player.getName() + teamBuilder.getSuffix() + "§7: §f" + message);
    }
}
