package fr.ziedelth.towers.listeners.entity;

import fr.ziedelth.towers.Towers;
import fr.ziedelth.towers.utils.Teams;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityExplodeEvent;

import java.util.List;
import java.util.stream.Collectors;

public class EntityExplode implements Listener {
    @EventHandler
    public void onEntityExplode(EntityExplodeEvent event) {
        List<Block> list = event.blockList();

        event.blockList().removeAll(list.stream().filter(block -> {
            for (Teams teams : Towers.getInstance().getTeams())
                return teams.getSpawnLocation().distance(block.getLocation()) <= Towers.getInstance().getTntSpawnProtectDistance();
            return false;
        }).collect(Collectors.toList()));
    }
}
