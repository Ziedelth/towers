package fr.ziedelth.towers;

import fr.ziedelth.towers.listeners.EventManager;
import fr.ziedelth.towers.teams.BlueTeam;
import fr.ziedelth.towers.teams.RedTeam;
import fr.ziedelth.towers.utils.*;
import fr.ziedelth.towers.utils.timers.EndManager;
import fr.ziedelth.towers.utils.timers.GameManager;
import fr.ziedelth.towers.utils.timers.LobbyManager;
import fr.ziedelth.towers.utils.timers.PreparationManager;
import org.apache.commons.io.FileUtils;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Towers extends JavaPlugin {
    private static Towers instance;
    private final LobbyManager lobbyManager = new LobbyManager();
    private final PreparationManager preparationManager = new PreparationManager();
    private final GameManager gameManager = new GameManager();
    private final EndManager endManager = new EndManager();
    private final List<Teams> teams = new ArrayList<>();
    private final int maxScore = 5;
    private final int minPlayers = 2;
    private final double tntSpawnProtectDistance = 15;
    private final double playersSpawnProtectDistance = 7;
    private Location lobby_location;

    public static Towers getInstance() {
        return instance;
    }

    /**
     * Méthode appelé lors du chargement du plugin
     */
    @Override
    public void onLoad() {
        super.onLoad();
        instance = this;
        // Définition du status de jeu
        GameStates.setState(GameStates.LOBBY);
    }

    /**
     * Méthode appelé lors de l'activation du plugin
     */
    @Override
    public void onEnable() {
        super.onEnable();
        // Chargement des minerais
        OreType.enable();
        // Chargement des évènements
        new EventManager();
        // Chargement des équipes
        this.addTeam(new RedTeam());
        this.addTeam(new BlueTeam());
        // Définition des coordonnées du lobby d'attentes
        this.lobby_location = new Location(Bukkit.getWorlds().get(0), 0.5, 64, 1024.5, 0f, 0f);

        // Initialisation des joueurs connectés
        Bukkit.getOnlinePlayers().forEach(player -> TowerPlayer.get(player).init());
    }

    /**
     * Méthode appelé lors de la désactivation du plugin
     */
    @Override
    public void onDisable() {
        super.onDisable();

        // Reset world
        try {
            World world = Bukkit.getWorlds().get(0);
            Bukkit.getServer().unloadWorld(world, false);
            FileUtils.copyDirectory(new File("Towers"), new File("world"));
            Bukkit.getServer().createWorld(new WorldCreator(world.getName()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Récupère le Timer utilisé selon le status de jeu
     *
     * @return Timer utilisé
     */
    public Timer getCurrentManager() {
        return (GameStates.isState(GameStates.LOBBY) ? this.lobbyManager : GameStates.isState(GameStates.PREPARATION) ? this.preparationManager : GameStates.isState(GameStates.GAME) ? this.gameManager : this.endManager);
    }

    public LobbyManager getLobbyManager() {
        return lobbyManager;
    }

    public PreparationManager getPreparationManager() {
        return preparationManager;
    }

    public GameManager getGameManager() {
        return gameManager;
    }

    public EndManager getEndManager() {
        return endManager;
    }

    /**
     * Ajoute une équipe au jeu
     *
     * @param team Equipe
     */
    public void addTeam(Teams team) {
        this.teams.add(team);
    }

    public List<Teams> getTeams() {
        return teams;
    }

    /**
     * Détecte si toutes les équipes ont le même nombre de joueurs
     *
     * @return Même nombre de joueurs
     */
    public boolean allTeamsHaveTheSameNumberPlayers() {
        int length = TowerPlayer.getPlayersInTeam(this.teams.get(0)).size();
        for (int i = 1; i < this.teams.size(); i++)
            if (TowerPlayer.getPlayersInTeam(this.teams.get(i)).size() != length) return false;
        return true;
    }

    /**
     * Récupère l'équipe qui a le moins de joueur
     *
     * @return Equipe
     */
    public Teams getSmallerTeam() {
        Teams team = null;
        int length = Integer.MAX_VALUE;

        for (int i = 0; i < this.teams.size(); i++) {
            Teams check = this.teams.get(i);
            int team_length = TowerPlayer.getPlayersInTeam(check).size();

            if (team_length < length) {
                length = team_length;
                team = check;
            }
        }

        return team;
    }

    /**
     * Récupère l'équipe qui a le plus de joueurs
     *
     * @return Equipe
     */
    public Teams getBiggerTeam() {
        Teams team = null;
        int length = Integer.MIN_VALUE;

        for (int i = 0; i < this.teams.size(); i++) {
            Teams check = this.teams.get(i);
            int team_length = TowerPlayer.getPlayersInTeam(check).size();

            if (team_length > length) {
                length = team_length;
                team = check;
            }
        }

        return team;
    }

    /**
     * Détecte si toutes les équipes ont le même score
     *
     * @return Même scores
     */
    public boolean allTeamsHaveTheSameScore() {
        int length = TowerPlayer.getScoreOf(this.teams.get(0));
        for (int i = 1; i < this.teams.size(); i++)
            if (TowerPlayer.getScoreOf(this.teams.get(i)) != length) return false;
        return true;
    }

    /**
     * Récupère l'équipe qui a le plus gros score
     *
     * @return Equipe
     */
    public Teams getTeamWithTheHighestScore() {
        Teams team = null;
        int length = Integer.MIN_VALUE;

        for (int i = 0; i < this.teams.size(); i++) {
            Teams check = this.teams.get(i);
            int team_score = TowerPlayer.getScoreOf(check);

            if (team_score > length) {
                length = team_score;
                team = check;
            }
        }

        return team;
    }

    public Location getLobbyLocation() {
        return lobby_location;
    }

    public int getMaxScore() {
        return maxScore;
    }

    public int getMinPlayers() {
        return minPlayers;
    }

    public double getTntSpawnProtectDistance() {
        return tntSpawnProtectDistance;
    }

    public double getPlayersSpawnProtectDistance() {
        return playersSpawnProtectDistance;
    }
}
